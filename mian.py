import cv2
import sys
import os
import time
import argparse
import requests


from detectFace import FaceDetector
from utility import annotate_image

def get(argW, thresh):
    face_detector = FaceDetector()

    if argW == "webcam":
        cap = cv2.VideoCapture(0)
    
    cv2.namedWindow("window", cv2.WND_PROP_FULLSCREEN)
    cv2.setWindowProperty("window", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

    now = time.time()
    while cap.isOpened():
        now = time.time()

        ret, frame = cap.read()

        if frame.shape[0] == 0:
            break

        rgv_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        if thresh:
            bboxes = face_detector.predict(rgv_frame, thresh)
        else:
            bboxes = face_detector.predict(rgv_frame)

        ann_frame = annotate_image(frame, bboxes)

        cv2.imshow('window', ann_frame)
        print("FPS: {:0.2f}".format(1 / (time.time() - now)), end="\r", flush=True)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    argument = sys.argv[1]
    if(argument == "source"):
        stream = requests.get('http://gkiaxis.informatik.privat/axis-cgi/mjpg/video.cgi?resolution=4CIF&color=1',auth=('gki', 'Felix&2'), stream=True)
        if (stream.status_code == 200):
            print("================================================================================")
            print("++++++++++++++Connected to network Camera, Now creating a pipeline++++++++++++++")
            print("================================================================================")
            bytes = bytes()
            for chunk in stream.iter_content(chunck_size = 1024):
                bytes += chunk
                a = bytes.find(b'\xff\xd8')
                b = bytes.find(b'\xff\xd9')
                if a!= -1 and b != -1:
                    jpg = bytes[a:b + 2]
                    bytes = bytes[b + 2:]
                    networkCamOutput = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8), cv2.IMREAD_COLOR)
                    print(networkCamOutput.shape)
                    get("webcam", 0.8)
                    
    else:
        get("webcam", 0.8)